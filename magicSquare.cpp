/*
 * magicSquare.cpp
 *
 *  Created on: Sep 1, 2014
 *      Author: Batterypowered7
 */

#include "magicSquare.h"

MagicSquare::MagicSquare()
{
   setnValue(FIVE);
   generateMagicSquare();
}

MagicSquare::MagicSquare(const int& n)
{
   setnValue(n);
   generateMagicSquare();
}

bool MagicSquare::isMagicSquare()
{
   bool flag = true;
   vector<int> values;
   vector<int> valuesSum;
   int upperBound = getnValue();
   int i;
   int j;
   int sum;

   for(i = 0; i < upperBound; ++i)
   {
      for(j = 0; j < upperBound; ++j)
      {
         values.push_back(getMagicSquareValues(i,j));
      }
   }

   //Sum all the numbers in each row. Store the results in a vector.
   for(i = 0; i < upperBound; ++i)
   {
      sum = 0;
      for(j = 0; j < upperBound; ++j)
      {
         sum += getMagicSquareValues(i,j);
      }
      valuesSum.push_back(sum);
   }

   //Sum all the numbers in each column. Store the results in a vector.
   for(i = 0; i < upperBound; ++i)
   {
      sum = 0;
      for(j = 0; j < upperBound; ++j)
      {
         sum += getMagicSquareValues(j, i);
      }
      valuesSum.push_back(sum);
   }

   //The following two loops sum the numbers along the diagonals and store the
   //results in the results vector.
   for (i = 0; i < upperBound; )
   {
      sum = 0;
      for (j = 0; j < upperBound; )
      {
         sum += getMagicSquareValues(i, j);
         ++i;
         ++j;
      }
      valuesSum.push_back(sum);
   }

   for(i = (upperBound - 1); i > 0; )
   {
      sum = 0;
      for(j = 0; j < upperBound; )
      {
         sum += getMagicSquareValues(i, j);
         --i;
         ++j;
      }
      valuesSum.push_back(sum);
   }

   //Compare each value in the results vector to the previous one. Any result
   //that is different from the rest will change flag to false.
   for(i = 1; i < valuesSum.size(); ++i)
   {
      if(valuesSum[i-1] != valuesSum[i])
      {
         flag = false;
      }
   }

   //Magic Squares must have all numbers between 1 and n^2. Test to make
   //sure all values are represented.
   for( i = 1; i <= (upperBound * upperBound); ++i)
   {
      int j = 0;
      for(; j < values.size(); ++j)
      {
         if(i == values[j])
         {
            flag = true;
            break;
         }
         else if (i != values[j])
         {
            flag = false;
         }
      }

   }

   return flag;
}

void MagicSquare::setnValue(const int& n)
{
   nValue = n;
}

int MagicSquare::getnValue()
{
   return nValue;
}

int MagicSquare::getMagicSquareValues(const int &x, const int &y)
{

   return magicSquare[x][y];

}

void MagicSquare::setMagicSquareValues(const int &x, const int &y, const int &n)
{
   magicSquare[x][y] = n;
}

//Deprecated function to print out the contents of a magic square.
//Replaced with an overloaded << operator.
void MagicSquare::printMagicSquare()
{

   cout << getnValue() << endl;

   for(int i = 0; i < getnValue(); ++i)
   {
      for(int j = 0; j < getnValue(); ++j)
      {
         cout << getMagicSquareValues(i,j) << " ";
      }

      cout << endl;
   }

   cout << endl;

}

MagicSquare::~MagicSquare()
{

}

void MagicSquare::generateBlankSquare()
{

   int upperBound = getnValue();
   for ( int i = 0; i < upperBound; i++ )
   {
      magicSquare.push_back ( vector<int>() );
      for ( int j = 0; j < upperBound; j++ )
      {
      magicSquare[i].push_back ( 0 );
      }
   }

}

void MagicSquare::generateMagicSquare()
{
   //upperBound is set to the nValue of the Magic Square. maxValue is
   //upperBound squared, as it will be the highest value in the Magic Square.
   //magicValue will act both as a counter and as the variable we use to fill
   //in the square.
   int upperBound = getnValue();
   int magicValue = 1;
   int maxValue = (upperBound * upperBound);

   generateBlankSquare();

   //Set initial conditions for where number placement is to begin. nextRow
   //and nextColumn will hold the next position while initRow and initColumn
   //will hold the previous position.
   int initRow = 0;
   int initColumn = upperBound/2;
   int nextRow;
   int nextColumn;

   //Begin instructions to fill out the Magic Square. Program will continue to
   //fill the Magic Square until it has reached the max value.
   while( magicValue < maxValue)
   {
      //Always begin in the top-center position.
      if(magicValue == 1)
      {
         magicSquare[initRow][initColumn] = magicValue;
         nextRow = upperBound - 1;
         nextColumn = initColumn +1;
         ++magicValue;
      }

      //Instructions for number placement if the next square is empty.
      if(magicSquare[nextRow][nextColumn] == 0)
      {
         magicSquare[nextRow][nextColumn] = magicValue;

         if ((nextRow - 1) < 0)
         {
            initRow = nextRow;
            nextRow = upperBound - 1;
         }

         else
         {
            initRow = nextRow;
            nextRow = nextRow - 1;
         }

         if ((nextColumn + 1) >= upperBound)
         {
            initColumn = nextColumn;
            nextColumn = 0;
         }

         else
         {
            initColumn = nextColumn;
            nextColumn = nextColumn + 1;
         }

         ++magicValue;

         //Special instructions in case the current value is the top-right corner.
         if(nextRow == 0 && nextColumn == upperBound - 1)
         {
            magicSquare[nextRow][nextColumn] = magicValue;
            nextRow = nextRow + 1;
            ++magicValue;
         }
      }

      //Instructions for number placement if the next spot is full.
      if (magicSquare[nextRow][nextColumn] != 0)
      {
         nextColumn = initColumn;
         nextRow = initRow + 1;
         magicSquare[nextRow][nextColumn] = magicValue;

         nextColumn = nextColumn + 1;
         nextRow = nextRow - 1;
         ++magicValue;
      }

      //Instruction for placement of the last value. Without this statement,
      //the program will break out of the loop without placing the final value
      //into the Magic Square.
      if(magicValue == maxValue)
      {
         magicSquare[nextRow][nextColumn] = magicValue;
      }

   }

}

MagicSquare MagicSquare::rotate()
{
   int upperBound = getnValue();

   //create a Magic Square. rSquare stands for "rotated square".
   MagicSquare rSquare(upperBound);

   //Clear the square.
   rSquare.generateBlankSquare();

   //Copy values of the square to be rotated into our empty square.
   for(int i = 0; i < upperBound; ++i)
   {
      for(int j = 0; j < upperBound; ++j)
      {
         rSquare.setMagicSquareValues(i, j, getMagicSquareValues(i, j));
      }
   }

   //Rotate these values in the square by 90 degrees.
   for(int i = 0; i < upperBound/2; ++i)
   {
      for(int j = 0; j < (upperBound + 1)/2; ++j)
      {
         int a = upperBound-1-j;
         int b = upperBound-1-i;
       int temp = rSquare.getMagicSquareValues(i,j);
         rSquare.setMagicSquareValues(i,j, rSquare.getMagicSquareValues(a,i));
         rSquare.setMagicSquareValues(a,i, rSquare.getMagicSquareValues(b,a));
         rSquare.setMagicSquareValues(b,a, rSquare.getMagicSquareValues(j,b));
         rSquare.setMagicSquareValues(j,b, temp);
      }
   }

   return rSquare;
}


ostream& operator<< (ostream &out, MagicSquare &mSquare)
{

   out << mSquare.getnValue() << endl;

   for(int i = 0; i < mSquare.getnValue(); ++i)
   {
      for(int j = 0; j < mSquare.getnValue(); ++j)
      {
         out << mSquare.getMagicSquareValues(i,j) << " ";
      }

      out << endl;
   }

   return out;

}

istream& operator>> (istream &in, MagicSquare &mSquare)
{

   int nValue;
   in >> nValue;
   mSquare.setnValue(nValue);
   mSquare.generateBlankSquare();
   for (int i = 0; i < nValue; ++i)
   {
      for (int j = 0; j < nValue; ++j)
      {
         int n = 0;
         in >> n;
         mSquare.setMagicSquareValues(i,j,n);
      }
   }

   return in;
}
