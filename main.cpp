/*
 * main.cpp
 *
 *  Created on: Sep 1, 2014
 *      Author: Batterypowered7
 */
#include "magicSquare.h"
#include <fstream>
#include <cstdlib>
#define WRITE 27

int main()
{

   int iValue = 7;
   int print = 0;
   int i;

   ofstream outClientFile("MagicSquares.dat", ios::out);

   if(!outClientFile)
   {
      cerr << "File could not be opened." << endl;
      exit( 1 );
   }

   MagicSquare defaultSquare;

   cout << "Testing the overloaded << operator" << endl;
   cout << defaultSquare << endl;

   if(defaultSquare.isMagicSquare())
   {
     outClientFile << defaultSquare << endl;;
   }

   if(!(defaultSquare.isMagicSquare()))
   {
      cout << "defaultSquare is not a magic square" << endl;
      cout << defaultSquare << endl;
   }

   MagicSquare rotatedDefaultSquare;

   rotatedDefaultSquare = defaultSquare.rotate();

   if(rotatedDefaultSquare.isMagicSquare())
   {
     outClientFile << rotatedDefaultSquare << endl;
   }

   if(!(rotatedDefaultSquare.isMagicSquare()))
   {
      cout << "rotatedDefaultSquare is not a Magic Square." << endl;
      cout << rotatedDefaultSquare << endl;
   }

   vector<MagicSquare> magicSquares;

   while(iValue < WRITE)
   {
      MagicSquare newSquare(iValue);
      magicSquares.push_back(newSquare);
      iValue += 2;

      MagicSquare rotatedSquare;
      rotatedSquare = newSquare.rotate();
      magicSquares.push_back(rotatedSquare);
      print += 2;
   }

   //Cycle through the magic squares in the vector. For each that are not true
   //Magic Squares, remove them from the vector and decrease the number of
   //items to be output to the file by one.
   for(i = 0; i < magicSquares.size(); ++i)
   {
     if(!(magicSquares[i].isMagicSquare()))
     {
        magicSquares.erase(magicSquares.begin() + i);
        --print;
     }
   }

   for(i = 0; i < print; ++i)
   {
      outClientFile << magicSquares[i] << endl;
   }

   MagicSquare cinSquare;

   cout << "Please specify the n size of the Magic Square, followed by the "
         "values: " << endl;

   cin >> cinSquare;
   cout << cinSquare;

   if(cinSquare.isMagicSquare())
   {
      cout << "The values you entered are a Magic Square!\n" << endl;
   }
   if(!(cinSquare.isMagicSquare()))
   {
      cout << "The values you entered are not a Magic Square!\n" << endl;
   }

   ifstream inClientFile( "input.dat", ios::in );
   vector <MagicSquare> inputSquares;
   MagicSquare inSquare;

   int instances;

   inClientFile >> instances;

   for(i = 0; i < instances; ++i)
   {
      inClientFile >> inSquare;
      inputSquares.push_back(inSquare);
   }

   for (i = 0; i < instances; ++i)
   {
      if(inputSquares[i].isMagicSquare())
      {
         cout << i << ": " << "is valid" << endl;
      }
      else if(!(inputSquares[i].isMagicSquare()))
      {
         cout << i << ": " << "is not valid" << endl;
      }
   }

}
