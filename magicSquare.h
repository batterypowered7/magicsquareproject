/*
 * magicSquare.h
 *
 *  Created on: Sep 1, 2014
 *      Author: Batterypowered7
 */

#ifndef MAGICSQUARE_H_
#define MAGICSQUARE_H_
#include <vector>
#include <iostream>
#include <iomanip>
using namespace std;

#define FIVE 5

class MagicSquare
{
public:

   MagicSquare();
   MagicSquare(const int& n);

   bool isMagicSquare();

   MagicSquare rotate();

   void setnValue(const int &n);
   int getnValue();

   void setMagicSquareValues(const int &x, const int &y, const int &n);
   int getMagicSquareValues(const int &x, const int &y);

   void printMagicSquare();
   ~MagicSquare();

   friend ostream& operator<< (ostream &out, MagicSquare &mSquare);
   friend istream& operator>> (istream &in, MagicSquare &mSquare);

private:

   void generateBlankSquare();
   void generateMagicSquare();
   int nValue;
   vector<vector<int> > magicSquare;

};
#endif /* MAGICSQUARE_H_ */
